<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="css/style.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">


		<div class="b-page__line">
			<div class="b-menu">
				<a href="places" class='b-link b-link_menu b-link_menu_active'>HOME</a>
				<a href="#" class='b-link b-link_menu'>ABOUT US</a> <a
					href="agreement.html" class='b-link b-link_menu'>SERVICES</a> <a
					href="#" class='b-link b-link_menu'>CUSTOMERS</a> <a href="#"
					class='b-link b-link_menu'>CONTACT</a>
			</div>
		</div>

		<div class="middle">

			<div class="container" id="1234">
				<main class="content" id="mainClass"> <c:forEach
					items="${textA}" var="element">
					<div id="${element.placeID}" class="parking-place">
						<c:choose>
							<c:when test="${element.avaliable==true}">
								<img src="source/green.png">
							</c:when>
							<c:when test="${element.avaliable==false}">
								<img src="source/red.png">
							</c:when>
							<c:otherwise>
								<img />
							</c:otherwise>
						</c:choose>
					</div>
				</c:forEach> </main>
				<!-- .content -->

			</div>
			<!-- .container-->

		</div>
		<!-- .middle-->


		<div class="footer">Footer</div>
	</div>
	<!-- .wrapper -->

	<div id="userInfo">

		Hello, <br> 
		${username}<br> 
		<br> 
		<form action="logout">
			<input type="submit" value="Logout">
		</form>
	</div>


	<div id="popup">

		<div class="info" id="sendform">
			<span class="popup-close" id='close'>x</span>
			<form method="post">
				First name:<br> <input type="text" name="firstname"
					pattern="[a-zA-Z]{4,}" required><br> <input
					type="checkbox" name="checkbox" required>I have read the <a
					href="agreement.html" target="_blank">user agreement</a>
				<button type="submit">Submit</button>
			</form>
		</div>
		<div class="info" id="error">
			Parking place is reserved
			<button class="popup-close">OK</button>
		</div>
	</div>

	<script src="js/popup.js" async></script>
	<script src="js/jquery-3.0.0.js"></script>
</body>
</html>