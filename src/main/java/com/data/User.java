package com.data;

public class User {

  public String Name;
  public String Login;
  public String Password;
  public User(String name, String login, String password) {
    super();
    Name = name;
    Login = login;
    Password = password;
  }
  public User() {
    super();
  }
  public synchronized String getName() {
    return Name;
  }
  public synchronized void setName(String name) {
    Name = name;
  }
  public synchronized String getLogin() {
    return Login;
  }
  public synchronized void setLogin(String login) {
    Login = login;
  }
  public synchronized String getPassword() {
    return Password;
  }
  public synchronized void setPassword(String password) {
    Password = password;
  }
  
  
  
}
