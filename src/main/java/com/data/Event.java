package com.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Event", propOrder={"name"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Event {
  
  @XmlElement(name="name")
  public String name;

  public Event(String name) {
    super();
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Event() {
    super();
  }
  
}
