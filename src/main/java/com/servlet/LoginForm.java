package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.dao.DataBaseDAO;
import com.servlet.data.SessionDataBean;

@Configurable
@WebServlet("/login")
public class LoginForm extends HttpServlet {


  @Autowired
  private DataBaseDAO dataBaseDAO;

  @Autowired
  private SessionDataBean sessionDataBean;
  /**
   * 
   */
  private static final long serialVersionUID = -3433056246720687220L;

  /**
   * 
   */

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  @Transactional(readOnly = false)
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String login = request.getParameter("login");
    String password = request.getParameter("password");
    String username = dataBaseDAO.getUser(login, password);

    if (username != null) {
      request.getSession();
      Cookie cookie = new Cookie("friendlyusername", username);
      response.addCookie(cookie);
      sessionDataBean.setLogin(login);
      sessionDataBean.setName(username);
      sessionDataBean.setPassword(password);
      sessionDataBean.setSessionId(request.getSession().getId());
      response.sendRedirect("/servletbased/places");
    } else {
      PrintWriter out = response.getWriter();
      out.print("<h1>User not found</h1><br>");
      out.print("Restration:<a href=\"/servletbased/registration\">link</a>");
    }
  }

}
