package com.servlet.listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

@WebListener
public class MyHttpSessionListener implements HttpSessionListener {
    private final static Logger log = Logger.getLogger(MyHttpSessionListener.class.getName());

    @Override
    public void sessionCreated(HttpSessionEvent e) {
        // Possibly create a stack trace here, and dump it
        log.info("Session created: " + e.getSession().getId() + ", timeout "
                + e.getSession().getMaxInactiveInterval());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent e) {
        log.info("Session destroyed: " + e.getSession().getId());
    }


}
