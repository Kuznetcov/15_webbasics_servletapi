package com.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.dao.DataBaseDAO;

@Configurable
@WebServlet("/registration")
public class Registration extends HttpServlet {

  final static Logger logger = Logger.getLogger("console");
  
  @Autowired
  private DataBaseDAO dataBaseDAO;
  /**
   * 
   */
  private static final long serialVersionUID = 3039999203319945807L;


  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    
    String login = request.getParameter("login");
    String password = request.getParameter("password");
    String name = request.getParameter("name");
    dataBaseDAO.addUser(name, login, password);
    response.sendRedirect("/servletbased/login");
  }

}
