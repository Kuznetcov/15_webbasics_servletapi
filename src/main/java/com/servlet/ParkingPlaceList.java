package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.dao.DataBaseDAO;
import com.data.ParkingPlace;
import com.servlet.data.SessionDataBean;

@Configurable
@WebServlet("/places")
public class ParkingPlaceList extends HttpServlet {

  @Autowired
  private DataBaseDAO dataBaseDAO;
  @Autowired
  private SessionDataBean sessionDataBean;
  /**
   * 
   */
  private static final long serialVersionUID = 7253944774193335175L;

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
  }


  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    String namePlaceholder = "%username";
    
    
    if (sessionDataBean.getSessionId()!=null && sessionDataBean.getSessionId().equals(request.getSession(false).getId())) {
      List<ParkingPlace> places = dataBaseDAO.getParkingPlaces();

      Cookie[] cookies = request.getCookies();
      for (int i = 0; i < cookies.length; i++) {
        String name = cookies[i].getName();
        if (name.equals("friendlyusername")) {
          namePlaceholder=cookies[i].getValue();
        }
      }
      request.setAttribute("textA", places);
      request.setAttribute("username", namePlaceholder);
      getServletContext().getRequestDispatcher("/places.jsp").forward(request, response);
    } else {
      PrintWriter out = response.getWriter();
      out.print("<h1>Session expired</h1><br>");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

  }

}
