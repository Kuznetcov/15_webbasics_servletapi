package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.dao.DataBaseDAO;
import com.data.Deal;

@Configurable
@WebServlet("/test")
public class TestServlet extends HttpServlet {

  @Autowired
  private DataBaseDAO dataBaseDAO;
  
  /**
   * 
   */
  private static final long serialVersionUID = -8484703512300398578L;

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
  }

  public TestServlet() {
    //
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    List<Deal> deals = dataBaseDAO.getDealsByClientID(1);
    PrintWriter out = response.getWriter();
    out.println("<h1>" + deals.size()+ "</h1>");
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
  }

}
