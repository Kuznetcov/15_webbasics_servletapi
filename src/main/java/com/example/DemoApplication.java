package com.example;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan
@EnableCaching
public class DemoApplication {

	public static void main(String[] args) {
		
	}
	
}
