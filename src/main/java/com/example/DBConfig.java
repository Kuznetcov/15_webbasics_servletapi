package com.example;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.dao.DataBaseDAO;
import com.dao.UseHibernate;

@org.springframework.context.annotation.Configuration
public class DBConfig {

  @Autowired
  PropertyPlaceholderConfigurer propertyPlaceholderConfigurer;
  
  @Autowired
  private SessionFactory sessionFactory;
  
  @Value("${DriverClassName}")
  private String driverClassName;
  @Value("${URL}")
  private String url;
  @Value("${name}")
  private String name;
  @Value("${Password}")
  private String password;
  
  @Value("${hibernate.dialect}")
  private String dialect;
  @Value("${hibernate.show_sql}")
  private String show_sql;
  @Value("${hibernate.hbm2ddl.auto}")
  private String hbm2ddl_auto;
  @Value("${hibernate.temp.use_jdbc_metadata_defaults}")
  private String metadata_defaults;
  
  
  
  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(driverClassName);
    dataSource.setUrl(url);
    dataSource.setUsername(name);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Bean
  public DataBaseDAO dataBaseDAO() {
    return new UseHibernate();
  }


  @Bean
  @Autowired
  public HibernateTemplate getHibernateTemplate() {
    HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
    return hibernateTemplate;
  }



  @Bean
  public TaskExecutor taskExecutor(){
    return new ThreadPoolTaskExecutor();
  }

}
