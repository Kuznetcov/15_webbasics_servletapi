package com.dao.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Client\"")
public class ClientDAO implements Serializable{
  
  /**
   * 
   */
  private static final long serialVersionUID = 5672476998795389120L;
  
  @Id 
  @Column(name = "id")  
  public Integer id;
  
  public String name;
  
  
  public synchronized Integer getId() {
    return id;
  }
  
  public synchronized void setId(Integer id) {
    this.id = id;
  }
  
  public synchronized String getName() {
    return name;
  }

  public synchronized void setName(String name) {
    this.name = name;
  }

  public ClientDAO() {}
  
}
