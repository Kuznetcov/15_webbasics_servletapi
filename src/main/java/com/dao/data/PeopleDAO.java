package com.dao.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"People\"")
public class PeopleDAO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 4177164913789589049L;

  
  @Id
  public Integer id;
  
  public String name;

  public synchronized Integer getId() {
    return id;
  }

  public synchronized void setId(Integer id) {
    this.id = id;
  }

  public synchronized String getName() {
    return name;
  }

  public synchronized void setName(String name) {
    this.name = name;
  }
  
  
  
}
