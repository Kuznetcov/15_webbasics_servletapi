package com.dao.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"EventType\"")
public class EventDAO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7129278636249394546L;
  
  @Id
  public String name;
  public boolean bloking;
  public boolean unbloking;

  public synchronized boolean getBloking() {
    return bloking;
  }


  public synchronized void setBloking(Boolean bloking) {
    this.bloking = bloking;
  }


  public synchronized boolean getUnblocking() {
    return unbloking;
  }


  public synchronized void setUnblocking(Boolean unblocking) {
    this.unbloking = unblocking;
  }


  public EventDAO() {
    super();
  }
  
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
