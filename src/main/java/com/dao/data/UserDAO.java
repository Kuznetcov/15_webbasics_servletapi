package com.dao.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "\"Users\"")
public class UserDAO {

  @Column(name = "name")
  public String name;
  @Id
  @Column(name = "login")
  public String login;
  @Column(name = "password")
  public String password;
  public UserDAO(String name, String login, String password) {
    super();
    this.name = name;
    this.login = login;
    this.password = password;
  }
  public UserDAO() {
    super();
  }
  public synchronized String getName() {
    return name;
  }
  public synchronized void setName(String name) {
    this.name = name;
  }
  public synchronized String getLogin() {
    return login;
  }
  public synchronized void setLogin(String login) {
    this.login = login;
  }
  public synchronized String getPassword() {
    return password;
  }
  public synchronized void setPassword(String password) {
    this.password = password;
  }
  
  
  
}