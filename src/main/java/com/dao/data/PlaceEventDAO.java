package com.dao.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "\"PlaceEvents\"")
public class PlaceEventDAO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5266794916299043598L;
  @Id
  @Column(name = "id")
  public Integer eventID;
  @Column(name = "place")
  public Integer placeID;
  @Column(name = "date")
  public Date date;
  @OneToOne
  @JoinColumn(name = "event", referencedColumnName = "name")
  public EventDAO event;


  public synchronized Integer getEventID() {
    return eventID;
  }

  public synchronized void setEventID(Integer eventID) {
    this.eventID = eventID;
  }

  public synchronized Integer getPlaceID() {
    return placeID;
  }

  public synchronized void setPlaceID(Integer placeID) {
    this.placeID = placeID;
  }

  public synchronized Date getDate() {
    return date;
  }

  public synchronized void setDate(Date date2) {
    this.date = date2;
  }

  public synchronized String getEvent() {
    return event.getName();
  }

  public synchronized void setEvent(EventDAO event) {
    this.event = event;
  }



}
