package com.dao;


import java.util.List;

import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.data.Client;
import com.data.Deal;
import com.data.Event;
import com.data.ParkingPlace;

public interface DataBaseDAO {

  public Client getClient(Integer id);
  
  public String addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber);
  
  public List<Deal> getDealsByClientID(Integer clientID);
  
  public String addPlaceEvent(Integer eventID, Integer placeID, String date, Event event);
  
  public List<Deal> getDeal(Deal deal);
  
  public String updateDealEmployee(Integer employeeIDFrom, Integer employeeIDTo);
  
  public List<ParkingPlace> getParkingPlaces();

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = false)
  public String addUser(String name, String login, String password);
  
  public String getUser(String login, String password);
}
